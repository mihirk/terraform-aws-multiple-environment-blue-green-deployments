resource "aws_key_pair" "default_keypair" {
  key_name = "${var.key_name}-${var.environment}"
  public_key = "${var.ssh_public_key}"
}

resource "aws_iam_user" "cluster" {
  name = "${var.environment}-user"
}

resource "aws_iam_access_key" "cluster_key" {
  user = "${aws_iam_user.cluster.name}"
}

resource "aws_iam_role" "cluster_host_role" {
  name = "${var.environment}_cluster_host_role"
  assume_role_policy = "${file("setup/cluster/policy/ecs-role.json")}"
}

resource "aws_iam_role_policy" "cluster_instance_role_policy" {
  name = "${var.environment}_cluster_instance_role_policy"
  policy = "${file("setup/cluster/policy/ecs-instance-role-policy.json")}"
  role = "${aws_iam_role.cluster_host_role.id}"
}

resource "aws_iam_role" "cluster_service_role" {
  name = "${var.environment}_cluster_service_role"
  assume_role_policy = "${file("setup/cluster/policy/ecs-role.json")}"
}

resource "aws_iam_role_policy" "cluster_service_role_policy" {
  name = "${var.environment}_cluster_service_role_policy"
  policy = "${file("setup/cluster/policy/ecs-service-role-policy.json")}"
  role = "${aws_iam_role.cluster_service_role.id}"
}

resource "aws_iam_instance_profile" "cluster_instance" {
  name = "${var.environment}_cluster_instance"
  path = "/"
  roles = [
    "${aws_iam_role.cluster_host_role.name}"]
}

resource "aws_iam_server_certificate" "ssl_certificate" {
  name = "${var.environment}_certificate"
  certificate_body = "${file("steup/cluster/certs/certificate.pem")}"
  private_key = "${file("steup/cluster/certs/private_key.pem")}"
}
