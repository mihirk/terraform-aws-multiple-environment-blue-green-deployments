resource "aws_launch_configuration" "cluster_instance" {
  name = "${var.environment}_cluster"
  image_id = "${var.ami}"
  instance_type = "${lookup(var.instance_type_map, var.environment, "t2.micro")}"
  key_name = "${aws_key_pair.default_keypair.key_name}"
  security_groups = [
    "${aws_security_group.cluster_sg.id}"]
  user_data = "#!/bin/bash\necho ECS_CLUSTER=${aws_ecs_cluster.cluster.name} > /etc/ecs/ecs.config"
  iam_instance_profile = "${aws_iam_instance_profile.cluster_instance.name}"
  associate_public_ip_address = true
  root_block_device {
    volume_size = "${var.disk_size}"
    volume_type = "gp2"
    delete_on_termination = false
  }
}

resource "aws_autoscaling_group" "cluster_ag" {
  name = "${var.environment}-asg"
  availability_zones = [
    "${var.region}a"]
  launch_configuration = "${aws_launch_configuration.cluster_instance.name}"
  min_size = "${lookup(var.min_scale_map, var.environment, 2)}"
  max_size = "${lookup(var.max_scale_map, var.environment, 4)}"
  desired_capacity = "${lookup(var.desired_scale_map, var.environment, 2)}"
  vpc_zone_identifier = [
    "${aws_subnet.main.id}"]
}

resource "aws_ecs_cluster" "cluster" {
  name = "${var.environment}-cluster"
}
