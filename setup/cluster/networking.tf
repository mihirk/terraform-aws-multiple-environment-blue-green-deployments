resource "aws_vpc" "vpc" {
  cidr_block = "${lookup(var.cidr_map, var.environment, format("172.%s.0.0/16", coalesce(var.random_cidr_key, var.cidr_key)))}"
  enable_dns_support = true
  enable_dns_hostnames = true
  enable_classiclink = false
  tags {
    Name = "${var.environment}"
  }
}

resource "aws_route_table" "external" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main.id}"
  }
}

resource "aws_route_table_association" "external_main" {
  subnet_id = "${aws_subnet.main.id}"
  route_table_id = "${aws_route_table.external.id}"
}

resource "aws_subnet" "main" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "${aws_vpc.vpc.cidr_block}"
  availability_zone = "${var.region}a"
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.vpc.id}"
}

resource "aws_security_group" "cluster_sg" {
  name = "${var.environment}-sg"
  description = "Container Instance Allowed Ports"
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "${aws_vpc.vpc.cidr_block}"
    ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags {
    Name = "${var.environment}-sg"
  }
}

resource "aws_security_group" "elb_sg" {
  name = "${var.environment}-lb-sg"
  description = "Load balancer Security Groups"
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags {
    Name = "${var.environment}-lb-sg"
  }
}